#!/usr/bin/env python

# Written by Bram Cohen and Myers Carpenter
# see LICENSE.txt for license information

from sys import argv, exit
from BitTorrent import version
from BitTorrent.download import download
from BitTorrent.spewout import print_spew
from threading import Event, Thread
from os.path import join, split, exists
from os import getcwd
import wxversion
wxversion.select("3.0")
import wx
from wx import EVT_BUTTON, EVT_CLOSE, EVT_LEFT_DOWN

from time import strftime, time
from webbrowser import open_new
from traceback import print_exc

def hours(n):
    if n == -1:
        return '<unknown>'
    if n == 0:
        return 'complete!'
    n = int(n)
    h, r = divmod(n, 60 * 60)
    m, sec = divmod(r, 60)
    if h > 1000000:
        return '<unknown>'
    if h > 0:
        return '%d hour %02d min %02d sec' % (h, m, sec)
    else:
        return '%d min %02d sec' % (m, sec)

wxEVT_INVOKE = wx.NewEventType()

def EVT_INVOKE(win, func):
    win.Connect(-1, -1, wxEVT_INVOKE, func)

class InvokeEvent(wx.PyEvent):
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_INVOKE)

class DownloadInfoFrame:
    def __init__(self, flag):
        frame = wx.Frame(None, -1, 'BitTorrent ' + version + ' download', size = wx.Size(400, 250))
        self.frame = frame
        self.flag = flag
        self.uiflag = Event()
        self.fin = False
        self.last_update_time = 0
        self.showing_error = False
        self.event = InvokeEvent()
        self.funclist = []

        panel = wx.Panel(frame, -1)
        colSizer = wx.FlexGridSizer(cols = 1, vgap = 3)

        fnsizer = wx.BoxSizer(wx.HORIZONTAL)

        self.fileNameText = wx.StaticText(panel, -1, '', style = wx.ALIGN_LEFT)
        fnsizer.Add(self.fileNameText, 1, wx.ALIGN_BOTTOM)
        self.aboutText = wx.StaticText(panel, -1, 'about', style = wx.ALIGN_RIGHT)
        self.aboutText.SetForegroundColour('Blue')
        self.aboutText.SetFont(wx.Font(14, wx.NORMAL, wx.NORMAL, wx.NORMAL, True))
        fnsizer.Add(self.aboutText, 0, wx.EXPAND)
        colSizer.Add(fnsizer, 0, wx.EXPAND)

        self.gauge = wx.Gauge(panel, -1, range = 1000, style = wx.GA_SMOOTH)
        colSizer.Add(self.gauge, 0, wx.EXPAND)

        gridSizer = wx.FlexGridSizer(cols = 2, vgap = 3, hgap = 8)
        
        gridSizer.Add(wx.StaticText(panel, -1, 'Estimated time left:'))
        self.timeEstText = wx.StaticText(panel, -1, '')
        gridSizer.Add(self.timeEstText, 0, wx.EXPAND)

        gridSizer.Add(wx.StaticText(panel, -1, 'Download to:'))
        self.fileDestText = wx.StaticText(panel, -1, '')
        gridSizer.Add(self.fileDestText, 0, wx.EXPAND)
        
        gridSizer.AddGrowableCol(1)

        rategridSizer = wx.FlexGridSizer(cols = 4, vgap = 3, hgap = 8)

        rategridSizer.Add(wx.StaticText(panel, -1, 'Download rate:'))
        self.downRateText = wx.StaticText(panel, -1, '')
        rategridSizer.Add(self.downRateText, 0, wx.EXPAND)

        rategridSizer.Add(wx.StaticText(panel, -1, 'Downloaded:'))
        self.downTotalText = wx.StaticText(panel, -1, '')
        rategridSizer.Add(self.downTotalText, 0, wx.EXPAND)
        
        rategridSizer.Add(wx.StaticText(panel, -1, 'Upload rate:'))
        self.upRateText = wx.StaticText(panel, -1, '')
        rategridSizer.Add(self.upRateText, 0, wx.EXPAND)
        
        
        rategridSizer.Add(wx.StaticText(panel, -1, 'Uploaded:'))
        self.upTotalText = wx.StaticText(panel, -1, '')
        rategridSizer.Add(self.upTotalText, 0, wx.EXPAND)
       
        rategridSizer.AddGrowableCol(1)
        rategridSizer.AddGrowableCol(3)

        
        colSizer.Add(gridSizer, 0, wx.EXPAND)
        colSizer.Add(rategridSizer, 0, wx.EXPAND)
        colSizer.Add((50, 50), 0, wx.EXPAND)
        self.cancelButton = wx.Button(panel, -1, 'Cancel')
        colSizer.Add(self.cancelButton, 0, wx.ALIGN_CENTER)
        colSizer.AddGrowableCol(0)
        colSizer.AddGrowableRow(3)

        border = wx.BoxSizer(wx.HORIZONTAL)
        border.Add(colSizer, 1, wx.EXPAND | wx.ALL, 4)
        panel.SetSizer(border)
        panel.SetAutoLayout(True)
        
        EVT_LEFT_DOWN(self.aboutText, self.donate)
        EVT_CLOSE(frame, self.done)
        EVT_BUTTON(frame, self.cancelButton.GetId(), self.done)
        EVT_INVOKE(frame, self.onInvoke)
        self.frame.SetIcon(wx.Icon('/usr/share/bittorrent/bittorrent.ico', wx.BITMAP_TYPE_ICO))
        self.frame.Show()

    def donate(self, event):
        Thread(target = self.donate2).start()

    def donate2(self):
        open_new('http://bitconjurer.org/BitTorrent/donate.html')

    def onInvoke(self, event):
        while not self.uiflag.isSet() and self.funclist:
            func, args, kwargs = self.funclist.pop(0)
            apply(func, args, kwargs)

    def invokeLater(self, func, args = [], kwargs = {}):
        if not self.uiflag.isSet():
            self.funclist.append((func, args, kwargs))
            if len(self.funclist) == 1:
                wx.PostEvent(self.frame, self.event)

    def updateStatus(self, d):
        if (self.last_update_time + 0.1 < time() and not self.showing_error) or d.get('fractionDone') in (0.0, 1.0) or d.has_key('activity'):
            self.invokeLater(self.onUpdateStatus, [d])

    def onUpdateStatus(self, d):
        try:
            if d.has_key('spew'):
                print_spew(d['spew'])
            activity = d.get('activity')
            fractionDone = d.get('fractionDone')
            timeEst = d.get('timeEst')
            downRate = d.get('downRate')
            upRate = d.get('upRate')
            downTotal = d.get('downTotal')
            upTotal = d.get('upTotal')
            if activity is not None and not self.fin:
                self.timeEstText.SetLabel(activity)
            if fractionDone is not None and not self.fin:
                self.gauge.SetValue(int(fractionDone * 1000))
                self.frame.SetTitle('%d%% %s - BitTorrent %s' % (int(fractionDone*100), self.filename, version))
            if timeEst is not None:
                self.timeEstText.SetLabel(hours(timeEst))
            if downRate is not None:
                self.downRateText.SetLabel('%.0f K/s' % (float(downRate) / (1 << 10)))
            if upRate is not None:
                self.upRateText.SetLabel('%.0f K/s' % (float(upRate) / (1 << 10)))
            if downTotal is not None:
                self.downTotalText.SetLabel('%.1f M' % (downTotal))
            if upTotal is not None:
                self.upTotalText.SetLabel('%.1f M' % (upTotal))
            self.frame.Refresh()
            self.frame.Update()
            self.last_update_time = time()
        except:
            print_exc()

    def finished(self):
        self.fin = True
        self.invokeLater(self.onFinishEvent)

    def failed(self):
        self.fin = True
        self.invokeLater(self.onFailEvent)

    def error(self, errormsg):
        if not self.showing_error:
            self.invokeLater(self.onErrorEvent, [errormsg])

    def onFinishEvent(self):
        self.timeEstText.SetLabel('Download Succeeded!')
        self.cancelButton.SetLabel('Close')
        self.gauge.SetValue(1000)
        self.frame.SetTitle('%s - Upload - BitTorrent %s' % (self.filename, version))
        self.downRateText.SetLabel('')

    def onFailEvent(self):
        self.timeEstText.SetLabel('Failed!')
        self.cancelButton.SetLabel('Close')
        self.gauge.SetValue(0)
        self.downRateText.SetLabel('')

    def onErrorEvent(self, errormsg):
        self.showing_error = True
        dlg = wx.MessageDialog(self.frame, message = errormsg, 
            caption = 'Download Error', style = wx.OK | wx.ICON_ERROR)
        dlg.Fit()
        dlg.Center()
        dlg.ShowModal()
        self.showing_error = False

    def chooseFile(self, default, size, saveas, dir):
        f = Event()
        bucket = [None]
        self.invokeLater(self.onChooseFile, [default, bucket, f, size, dir, saveas])
        f.wait()
        return bucket[0]
    
    def onChooseFile(self, default, bucket, f, size, dir, saveas):
        if not saveas:
            if dir:
                dl = wx.DirDialog(self.frame, 'Choose a directory to save to, pick a partial download to resume', 
                    join(getcwd(), default), style = wx.DD_DEFAULT_STYLE | wx.DD_NEW_DIR_BUTTON)
            else:
                dl = wx.FileDialog(self.frame, 'Choose file to save as, pick a partial download to resume', '', default, '*', wx.FD_SAVE)
            if dl.ShowModal() != wx.ID_OK:
                self.done(None)
                f.set()
                return
            saveas = dl.GetPath()
        bucket[0] = saveas
        self.fileNameText.SetLabel('%s (%.1f M)' % (default, float(size) / (1 << 20)))
        self.timeEstText.SetLabel('Starting up...')
        self.fileDestText.SetLabel(saveas)
        self.filename = default
        self.frame.SetTitle(default + '- BitTorrent ' + version)
        f.set()

    def newpath(self, path):
        self.fileDestText.SetLabel(path)

    def done(self, event):
        self.uiflag.set()
        self.flag.set()
        self.frame.Destroy()

class btWxApp(wx.App):
    def __init__(self, x, params):
        self.params = params
        wx.App.__init__(self, x)

    def OnInit(self):
        doneflag = Event()
        d = DownloadInfoFrame(doneflag)
        self.SetTopWindow(d.frame)
        thread = Thread(target = next, args = [self.params, d, doneflag])
        thread.setDaemon(False)
        thread.start()
        return 1

def run(params):
    try:
        app = btWxApp(0, params)
        app.MainLoop()
    except:
        print_exc()

def next(params, d, doneflag):
    try:
        download(params, d.chooseFile, d.updateStatus, d.finished, d.error, doneflag, 100, d.newpath)
        if not d.fin:
            d.failed()
    except:
        print_exc()

if __name__ == '__main__':
    run(argv[1:])
