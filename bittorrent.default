# Default configuration for bittorrent tracker, bttrack

# If you want the bittorrent tracker to run, switch this to 1.
# If you change this, you will probably want to change 
# ALLOWED_DIR as well, or anyone will be able to track anything
# just by pointing the .torrent at your server.
START_BTTRACK=0

# Set any bttrack option --foo by defining the variable FOO to the argument
# you'd like to pass with the --foo option.  See `man bttrack` for a detailed
# discussion of the options.

# Persistent state file
DFILE=/var/lib/bittorrent/bttrack.state

# Port defaults to 80, which tends to be inconvenient
PORT=6969

# Only allow downloads for .torrent files in this directory
#ALLOWED_DIR=/srv/ftp

# The following options do not correspond to bttrack options; they influence how
# bttrack's init script starts the daemon.

# Run under this uid.  Must have access to all files and directories involved,
# naturally, but should otherwise have minimal privileges to minimize any
# security risk.
#DAEMONUSER=bittorrent

# chroot to this directory before starting the daemon.  This can also help keep
# the daemon secure, but may interact with all sorts of file locations in
# unexpected ways.
#DAEMONCHROOT=/var/local/lib/bttrack-sandbox

# Move to this directory before starting the daemon.  This may be useful in
# conjunction with DAEMONCHROOT.
#DAEMONCHDIR=/var/local/lib/bttrack-sandbox

# Run the daemon at this "nice" priority.  Setting a positive value here will
# dissuade the system from giving all its CPU time to bttrack requests from the
# network.
DAEMONNICE=5

# Append log output from daemon to this file.  Make sure this log is rotated
# from time to time so it doesn't fill up your disk.  The daemon will of course
# need write access to the log file.
DAEMONLOGFILE=/var/log/bittorrent/bttrack.log
