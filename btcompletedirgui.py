#!/usr/bin/env python

# Written by Bram Cohen
# see LICENSE.txt for license information

from sys import argv, version

from BitTorrent.btcompletedir import completedir
from threading import Event, Thread
from os.path import join, split
from sys import argv
import wxversion
wxversion.select("3.0")
import wx
from wx import EVT_BUTTON, EVT_CLOSE

from traceback import print_exc

wxEVT_INVOKE = wx.NewEventType()

def EVT_INVOKE(win, func):
    win.Connect(-1, -1, wxEVT_INVOKE, func)

class InvokeEvent(wx.PyEvent):
    def __init__(self, func, args, kwargs):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_INVOKE)
        self.func = func
        self.args = args
        self.kwargs = kwargs

class DownloadInfo:
    def __init__(self):
        frame = wx.Frame(None, -1, 'BitTorrent complete dir 1.1', size = wx.Size(550, 250))
        self.frame = frame
        frame.SetIcon(wx.Icon('/usr/share/bittorrent/bittorrent.ico', wx.BITMAP_TYPE_ICO))

        panel = wx.Panel(frame, -1)

        gridSizer = wx.FlexGridSizer(cols = 2, vgap = 15, hgap = 8)
        
        gridSizer.Add(wx.StaticText(panel, -1, 'Files to make .torrents for:'))
        self.dirCtl = wx.TextCtrl(panel, -1, '')

        b = wx.BoxSizer(wx.HORIZONTAL)
        b.Add(self.dirCtl, 1, wx.EXPAND)
        b.Add((10, 10), 0, wx.EXPAND)
        button = wx.Button(panel, -1, 'add file')
        b.Add(button, 0, wx.EXPAND)
        EVT_BUTTON(frame, button.GetId(), self.select)
        b.Add((5, 5), 0, wx.EXPAND)
        c = wx.Button(panel, -1, 'add dir')
        b.Add(c, 0, wx.EXPAND)
        EVT_BUTTON(frame, c.GetId(), self.selectdir)

        gridSizer.Add(b, 0, wx.EXPAND)

        gridSizer.Add(wx.StaticText(panel, -1, 'announce url:'))
        self.annCtl = wx.TextCtrl(panel, -1, 'http://my.tracker:6969/announce')
        gridSizer.Add(self.annCtl, 0, wx.EXPAND)

        gridSizer.Add(wx.StaticText(panel, -1, 'piece size:'))
        self.piece_length = wx.Choice(panel, -1, choices = ['2 ** 21', '2 ** 20', '2 ** 19', 
            '2 ** 18', '2 ** 17', '2 ** 16', '2 ** 15'])
        self.piece_length.SetSelection(3)
        gridSizer.Add(self.piece_length)

        gridSizer.AddGrowableCol(1)
 
        border = wx.BoxSizer(wx.VERTICAL)
        border.Add(gridSizer, 0, wx.EXPAND | wx.NORTH | wx.EAST | wx.WEST, 25)
        b2 = wx.Button(panel, -1, 'make')
        border.Add((10, 10), 1, wx.EXPAND)
        border.Add(b2, 0, wx.ALIGN_CENTER | wx.SOUTH, 20)
        EVT_BUTTON(frame, b2.GetId(), self.complete)
        panel.SetSizer(border)
        panel.SetAutoLayout(True)

    def select(self, x):
        dl = wx.FileDialog(self.frame, "Choose files", '/', "", '*.*', wx.FD_OPEN | wx.FD_MULTIPLE)
        if dl.ShowModal() == wx.ID_OK:
            x = self.dirCtl.GetValue() + ';' + ';'.join(dl.GetPaths())
            if x[0] == ';':
                x = x[1:]
            self.dirCtl.SetValue(x)

    def selectdir(self, x):
        dl = wx.DirDialog(self.frame, 'Choose directories', '/')
        if dl.ShowModal() == wx.ID_OK:
            x = self.dirCtl.GetValue() + ';' + dl.GetPath()
            if x[0] == ';':
                x = x[1:]
            self.dirCtl.SetValue(x)

    def complete(self, x):
        if self.dirCtl.GetValue() == '':
            dlg = wx.MessageDialog(self.frame, message = 'You must select a directory', 
                caption = 'Error', style = wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()
            return
        try:
            ps = 21 - self.piece_length.GetSelection()
            CompleteDir(self.dirCtl.GetValue().split(';'), self.annCtl.GetValue(), ps)
        except:
            print_exc()
            sleep(50000)

class CompleteDir:
    def __init__(self, d, a, pl):
        self.d = d
        self.a = a
        self.pl = pl
        self.flag = Event()
        frame = wx.Frame(None, -1, 'BitTorrent make .torrent', size = wx.Size(550, 250))
        self.frame = frame

        panel = wx.Panel(frame, -1)

        gridSizer = wx.FlexGridSizer(cols = 1, vgap = 15, hgap = 8)

        self.currentLabel = wx.StaticText(panel, -1, 'checking file sizes')
        gridSizer.Add(self.currentLabel, 0, wx.EXPAND)
        self.gauge = wx.Gauge(panel, -1, range = 1000, style = wx.GA_SMOOTH)
        gridSizer.Add(self.gauge, 0, wx.EXPAND)
        gridSizer.Add((10, 10), 1, wx.EXPAND)
        self.button = wx.Button(panel, -1, 'cancel')
        gridSizer.Add(self.button, 0, wx.ALIGN_CENTER)
        gridSizer.AddGrowableRow(2)
        gridSizer.AddGrowableCol(0)

        g2 = wx.FlexGridSizer(cols = 1, vgap = 15, hgap = 8)
        g2.Add(gridSizer, 1, wx.EXPAND | wx.ALL, 25)
        g2.AddGrowableRow(0)
        g2.AddGrowableCol(0)
        panel.SetSizer(g2)
        panel.SetAutoLayout(True)
        EVT_BUTTON(frame, self.button.GetId(), self.done)
        EVT_CLOSE(frame, self.done)
        EVT_INVOKE(frame, self.onInvoke)
        frame.Show(True)
        Thread(target = self.complete).start()

    def complete(self):
        try:
            completedir(self.d, self.a, self.flag, self.valcallback, self.filecallback, self.pl)
            if not self.flag.isSet():
                self.currentLabel.SetLabel('Done!')
                self.gauge.SetValue(1000)
                self.button.SetLabel('Close')
        except (OSError, IOError), e:
            self.currentLabel.SetLabel('Error!')
            self.button.SetLabel('Close')
            dlg = wx.MessageDialog(self.frame, message = 'Error - ' + str(e), 
                caption = 'Error', style = wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()

    def valcallback(self, amount):
        self.invokeLater(self.onval, [amount])

    def onval(self, amount):
        self.gauge.SetValue(int(amount * 1000))

    def filecallback(self, f):
        self.invokeLater(self.onfile, [f])

    def onfile(self, f):
        self.currentLabel.SetLabel('building ' + join(self.d, f) + '.torrent')

    def onInvoke(self, event):
        if not self.flag.isSet():
            apply(event.func, event.args, event.kwargs)

    def invokeLater(self, func, args = [], kwargs = {}):
        if not self.flag.isSet():
            wx.PostEvent(self.frame, InvokeEvent(func, args, kwargs))

    def done(self, event):
        self.flag.set()
        self.frame.Destroy()

class btWxApp(wx.App):
    def OnInit(self):
        d = DownloadInfo()
        d.frame.Show(True)
        self.SetTopWindow(d.frame)
        return True

if __name__ == '__main__':
    btWxApp().MainLoop()

